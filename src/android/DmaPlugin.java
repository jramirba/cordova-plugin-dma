package com.dma.cordova.plugin;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.Toast;


import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.mbte.dialmyapp.messages.fcm.FcmHandler;
import org.mbte.dialmyapp.sms.DmaVerificationBySmsProvider;
import org.mbte.dialmyapp.util.AutoStartHelper;
import org.mbte.dialmyapp.util.XiaomiUtils;
import org.mbte.dialmyapp.util.RequestSendPrivateInfoHelper;

public class DmaPlugin extends CordovaPlugin {
    private static final String ACTION_SHOW_TOAST = "showToast";

    private static final String ACTION_IS_AUTOSTART_DETECTED = "isAutostartDetected";
    private static final String ACTION_HAS_AUTOSTART_EVER_REQUESTED = "hasAutostartEverRequested";
    private static final String ACTION_REQUEST_AUTOSTART = "requestAutostart";
    private static final String ACTION_REQUEST_AUTOSTART_AUTO = "requestAutostartAuto";
    private static final String ACTION_CLEAR_REQUEST_AUTOSTART = "clearRequestAutostart";

    private static final String ACTION_IS_XIAOMI_PERMS_DETECTED = "isXiaomiPermsDetected";
    private static final String ACTION_HAVE_XIAOMI_PERMS_EVER_REQUESTED = "haveXiaomiPermsEverRequested";
    private static final String ACTION_REQUEST_XIAOMI_PERMS = "requestXiaomiPerms";
    private static final String ACTION_REQUEST_XIAOMI_PERMS_AUTO = "requestXiaomiPermsAuto";
    private static final String ACTION_CLEAR_REQUEST_XIAOMI_PERMS = "clearRequestXiaomiPerms";

    private static final String ACTION_IS_OVERLAY_REQUIRED = "isOverlayRequired";
    private static final String ACTION_IS_OVERLAY_GRANTED = "isOverlayGranted";
    private static final String ACTION_REQUEST_OVERLAY = "requestOverlay";

    private static final String ACTION_ON_FCM_TOKEN_RECEIVED = "onFcmTokenReceived";
    private static final String ACTION_ON_FCM_MESSAGE_RECEIVED = "onFcmMessageReceived";

    private static final String ACTION_IS_PROMINENT_APPROVED = "isProminentApproved";
    private static final String ACTION_SET_PROMINENT_APPROVED = "setProminentApproved";

    private static final String ACTION_OPEN_PROFILE = "openProfile";

    private static final String ACTION_IS_VERIFIED_NUMBER_SET = "isVerifiedNumberSet";
    private static final String ACTION_SET_VERIFIED_NUMBER = "setVerifiedNumber";

    private static final String DURATION_LONG = "long";
    private CallbackContext permsCallbackContext = null;
    private String afterRequestPermTag = null;
    private CallbackContext callbackContext = null;

    @Override
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) {
        try {
            if (ACTION_IS_AUTOSTART_DETECTED.equals(action)) {
                isAutoStartDetected(callbackContext);
            } else if (ACTION_HAS_AUTOSTART_EVER_REQUESTED.equals(action)) {
                hasAutoStartEverRequested(callbackContext);
            } else if (ACTION_REQUEST_AUTOSTART.equals(action)) {
                permsCallbackContext = callbackContext;
                afterRequestPermTag = "autostart";
                requestAutoStart(callbackContext);
            } else if (ACTION_REQUEST_AUTOSTART_AUTO.equals(action)) {
                requestAutoStartAuto(callbackContext);
            } else if (ACTION_CLEAR_REQUEST_AUTOSTART.equals(action)) {
                clearRequestAutoStart(callbackContext);
            } else if (ACTION_SHOW_TOAST.equals(action)) {
                showToast(args, callbackContext);
            } else if (ACTION_IS_XIAOMI_PERMS_DETECTED.equals(action)) {
                isXiaomiPermsDetected(callbackContext);
            } else if (ACTION_HAVE_XIAOMI_PERMS_EVER_REQUESTED.equals(action)) {
                haveXiaomiPermsEverRequested(callbackContext);
            } else if (ACTION_REQUEST_XIAOMI_PERMS.equals(action)) {
                permsCallbackContext = callbackContext;
                afterRequestPermTag = "xiaomi";
                requestXiaomiPerms(args, callbackContext);
            } else if (ACTION_REQUEST_XIAOMI_PERMS_AUTO.equals(action)) {
                requestXiaomiPermsAuto(args, callbackContext);
            } else if (ACTION_CLEAR_REQUEST_XIAOMI_PERMS.equals(action)) {
                clearRequestXiaomiPerms(callbackContext);
            } else if (ACTION_ON_FCM_TOKEN_RECEIVED.equals(action)) {
				this.callbackContext = callbackContext;
                onFcmTokenReceived(callbackContext, args);
            } else if (ACTION_ON_FCM_MESSAGE_RECEIVED.equals(action)) {
                onFcmMessageReceived(callbackContext, args);
            } else if (ACTION_OPEN_PROFILE.equals(action)) {
                openProfile(callbackContext, args);
            } else if (ACTION_IS_VERIFIED_NUMBER_SET.equals(action)) {
                isVerifiedNumberSet(callbackContext);
            } else if (ACTION_SET_VERIFIED_NUMBER.equals(action)) {
                setVerifiedNumber(callbackContext, args);
            } else if (ACTION_IS_OVERLAY_REQUIRED.equals(action)) {
                isOverlayRequired(callbackContext);
            } else if (ACTION_IS_OVERLAY_GRANTED.equals(action)) {
                isOverlayGranted(callbackContext);
            } else if (ACTION_REQUEST_OVERLAY.equals(action)) {
                requestOverlay(callbackContext);
            } else if (ACTION_IS_PROMINENT_APPROVED.equals(action)) {
                isProminentApproved(callbackContext);
            } else if (ACTION_SET_PROMINENT_APPROVED.equals(action)) {
                setProminentApproved(callbackContext);
            } else{
                callbackContext.error("\"" + action + "\" is not a recognized action.");
                return false;
            }
        } catch (Exception e) {
            callbackContext.error(action + ": " + e.getLocalizedMessage());
        }
        return true;
    }

    private void openProfile(CallbackContext callbackContext, JSONArray args) {
        String name = "";
        try {
            JSONObject options = args.getJSONObject(0);
            name = options.getString("name");
        } catch (JSONException e) {
            callbackContext.error("DMA: openProfile: Error: " + e.getMessage());
        }
        if (name.isEmpty()) {
            callbackContext.error("DMA: openProfile: name is empty");
        }
        Intent intent = new Intent();
        intent.setClassName(cordova.getActivity(), "org.mbte.dialmyapp.activities.LoadProfileActivity");
        intent.putExtra("profile", "" + name);
        cordova.getActivity().startActivity(intent);
        callbackContext.success();
    }
	
	private void onFcmMessageReceived(CallbackContext callbackContext, JSONArray args) throws JSONException {
        String payload = args.getJSONObject(0).getString("payload")
                .replace("\\", "")
                .replace("\"{", "{")
                .replace("}\"", "}");
        FcmHandler.onNewMessageReceived(cordova.getActivity(), payload);
        callbackContext.success();
    }

    private void onFcmTokenReceived(CallbackContext callbackContext, JSONArray args) {
        String token = null;
        try {
            JSONObject options = args.getJSONObject(0);
            token = options.getString("token");
        } catch (JSONException e) {
            callbackContext.error("DMA: OnFcmTokenReceived: Error: " + e.getMessage());
        }
        FcmHandler.onTokenReceived(cordova.getActivity(), token, new Bundle(), new FcmHandler.OnSubscribePushesListener() {
            @Override
            public void onSuccess() {
                PluginResult result = new PluginResult(PluginResult.Status.OK, "DMA: OnFcmSubscribed: success");
                result.setKeepCallback(false);
                releaseCallback(result);
            }

            @Override
            public void onError() {
                PluginResult result = new PluginResult(PluginResult.Status.ERROR, "DMA: OnFcmSubscribed: error");
                result.setKeepCallback(false);
                releaseCallback(result);
            }
        });

        onKeepCallback(callbackContext);
    }

    private void onKeepCallback(CallbackContext callbackContext) {
        PluginResult result = new PluginResult(PluginResult.Status.NO_RESULT);
        result.setKeepCallback(true);
        callbackContext.sendPluginResult(result);
    }

    private void releaseCallback(PluginResult result) {
        if (this.callbackContext != null && result != null) {
            this.callbackContext.sendPluginResult(result);
            this.callbackContext = null;
        }
    }

    private void isXiaomiPermsDetected(CallbackContext callbackContext) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("result", XiaomiUtils.isXiaomi());
        callbackContext.success(jsonObject);
    }

    private void haveXiaomiPermsEverRequested(CallbackContext callbackContext) throws JSONException {
        if (XiaomiUtils.isXiaomi()) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("result", XiaomiUtils.hasEverRequestedManually(cordova.getActivity()));
            callbackContext.success(jsonObject);
        } else {
            callbackContext.error("No need to request Xiaomi permissions on your device");
        }
    }

    private void requestXiaomiPerms(JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (XiaomiUtils.isXiaomi()) {
            boolean showToast = args.getJSONObject(0).optBoolean("showToast", true);
            XiaomiUtils.newInstance(cordova.getActivity()).queryXiaomiOtherPermissions(cordova.getActivity(), showToast);
            onKeepCallback(callbackContext);
        } else {
            callbackContext.error("No need to request Xiaomi permissions on your device");
        }
    }

    private void requestXiaomiPermsAuto(JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (XiaomiUtils.isXiaomi()) {
            if (XiaomiUtils.hasEverRequestedManually(cordova.getActivity())) {
                callbackContext.error("Xiaomi permissions had been requested already");
            } else {
                boolean showToast = args.getJSONObject(0).optBoolean("showToast", true);
                XiaomiUtils.newInstance(cordova.getActivity()).queryXiaomiOtherPermissions(cordova.getActivity(), showToast);
                callbackContext.success();
            }
        } else {
            callbackContext.error("No need to request Xiaomi permissions on your device");
        }
    }

    private void clearRequestXiaomiPerms(CallbackContext callbackContext) {
        XiaomiUtils.clearHasEverRequestedManually(cordova.getActivity());
        callbackContext.success();
    }

    private void isAutoStartDetected(CallbackContext callbackContext) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("result", AutoStartHelper.isAutoStartDetected(cordova.getActivity()));
        callbackContext.success(jsonObject);
    }

    private void hasAutoStartEverRequested(CallbackContext callbackContext) throws JSONException {
        if (AutoStartHelper.isAutoStartDetected(cordova.getActivity())) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("result", AutoStartHelper.hasEverRequested(cordova.getActivity()));
            callbackContext.success(jsonObject);
        } else {
            callbackContext.error("No need to request AutoStart on your device");
        }
    }

    private void requestAutoStart(CallbackContext callbackContext) {
        if (AutoStartHelper.isAutoStartDetected(cordova.getActivity())) {
            AutoStartHelper.simpleRequestAutoStart(cordova.getActivity());
            onKeepCallback(callbackContext);
        } else {
            callbackContext.error("No need to request AutoStart on your device");
        }
    }

    private void requestAutoStartAuto(CallbackContext callbackContext) {
        if (AutoStartHelper.isAutoStartDetected(cordova.getActivity())) {
            if (AutoStartHelper.hasEverRequested(cordova.getActivity())) {
                callbackContext.error("AutoStart was already requested");
            } else {
                AutoStartHelper.simpleRequestAutoStart(cordova.getActivity());
                callbackContext.success();
            }
        } else {
            callbackContext.error("No need to request AutoStart on your device");
        }
    }

    private void clearRequestAutoStart(CallbackContext callbackContext) {
        AutoStartHelper.clearHasEverRequested(cordova.getActivity());
        callbackContext.success();
    }

    private void showToast(JSONArray args, CallbackContext callbackContext) {
        String message = null;
        String duration = null;
        try {
            JSONObject options = args.getJSONObject(0);
            message = options.getString("message");
            duration = options.getString("duration");
        } catch (JSONException e) {
            callbackContext.error("showToast: " + e.getMessage());
        }
        Toast.makeText(cordova.getActivity(), message, DURATION_LONG.equals(duration) ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT).show();
        callbackContext.success();
    }

    @Override
    public void onResume(boolean multitasking) {
        super.onResume(multitasking);
        checkRequestingPermsCallback();
    }

    private void checkRequestingPermsCallback() {
        if (permsCallbackContext != null) {
            PluginResult result = new PluginResult(PluginResult.Status.OK, afterRequestPermTag);
            result.setKeepCallback(false);
            if (this.permsCallbackContext != null && result != null) {
                this.permsCallbackContext.sendPluginResult(result);
                this.permsCallbackContext = null;
            }
        }
    }

    private void isOverlayRequired(CallbackContext callbackContext) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("result", Build.VERSION.SDK_INT >= Build.VERSION_CODES.P);
        callbackContext.success(jsonObject);
    }

    private void requestOverlay(CallbackContext callbackContext) {
        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + cordova.getActivity().getPackageName()));
        cordova.getActivity().startActivity(intent);
        callbackContext.success();
    }

    private void isOverlayGranted(CallbackContext callbackContext) throws JSONException {
        boolean isGranted = Build.VERSION.SDK_INT < Build.VERSION_CODES.M || Settings.canDrawOverlays(cordova.getActivity());
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("result", isGranted);
        callbackContext.success(jsonObject);
    }
	
	public void setVerifiedNumber(CallbackContext callbackContext, JSONArray args) {
		String phoneNumber = "";
        try {
            JSONObject options = args.getJSONObject(0);
            phoneNumber = options.getString("pn");
        } catch (JSONException e) {
            callbackContext.error("DMA: setVerifiedNumber: Error: " + e.getMessage());
        }
        if (phoneNumber.isEmpty()) {
            callbackContext.error("DMA: setVerifiedNumber: phoneNumber is empty");
        }
		
		
        DmaVerificationBySmsProvider.setVerifiedNumber(cordova.getActivity(), phoneNumber);
		callbackContext.success();
    }

    private void isVerifiedNumberSet(CallbackContext callbackContext) throws JSONException {
		String pn = DmaVerificationBySmsProvider.getVerifiedNumber(cordova.getActivity());
		JSONObject jsonObject = new JSONObject();
        jsonObject.put("result", !pn.isEmpty());
        callbackContext.success(jsonObject);
    }

    private void isProminentApproved(CallbackContext callbackContext) throws JSONException{
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("result", RequestSendPrivateInfoHelper.isSendPrivateInfoApproved(cordova.getActivity()));
        callbackContext.success(jsonObject);
    }

    private void setProminentApproved(CallbackContext callbackContext) {
        RequestSendPrivateInfoHelper.setSendPrivateInfoValue(cordova.getActivity(), true);
        callbackContext.success();
    }
}
