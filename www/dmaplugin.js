// Empty constructor
function DmaPlugin() {}

// The function that passes work along to native shells
// Message is a string, duration may be 'long' or 'short'
DmaPlugin.prototype.showToast = function(message, duration, successCallback, errorCallback) {
  var options = {};
  options.message = message;
  options.duration = duration;
  cordova.exec(successCallback, errorCallback, 'DmaPlugin', 'showToast', [options]);
}

DmaPlugin.prototype.isAutostartDetected = function(successCallback, errorCallback) {
  var options = {};
  cordova.exec(successCallback, errorCallback, 'DmaPlugin', 'isAutostartDetected', [options]);
}

DmaPlugin.prototype.isXiaomiPermsDetected = function(successCallback, errorCallback) {
  var options = {};
  cordova.exec(successCallback, errorCallback, 'DmaPlugin', 'isXiaomiPermsDetected', [options]);
}

DmaPlugin.prototype.hasAutostartEverRequested = function(successCallback, errorCallback) {
  var options = {};
  cordova.exec(successCallback, errorCallback, 'DmaPlugin', 'hasAutostartEverRequested', [options]);
}

DmaPlugin.prototype.haveXiaomiPermsEverRequested = function(successCallback, errorCallback) {
  var options = {};
  cordova.exec(successCallback, errorCallback, 'DmaPlugin', 'haveXiaomiPermsEverRequested', [options]);
}

DmaPlugin.prototype.requestAutostart = function(successCallback, errorCallback) {
  var options = {};
  cordova.exec(successCallback, errorCallback, 'DmaPlugin', 'requestAutostart', [options]);
}

DmaPlugin.prototype.requestXiaomiPerms = function(showToast, successCallback, errorCallback) {
  var options = {};
  options.showToast = showToast;
  cordova.exec(successCallback, errorCallback, 'DmaPlugin', 'requestXiaomiPerms', [options]);
}

DmaPlugin.prototype.requestAutostartAuto = function(successCallback, errorCallback) {
  var options = {};
  cordova.exec(successCallback, errorCallback, 'DmaPlugin', 'requestAutostartAuto', [options]);
}

DmaPlugin.prototype.requestXiaomiPermsAuto = function(showToast, successCallback, errorCallback) {
  var options = {};
  options.showToast = showToast;
  cordova.exec(successCallback, errorCallback, 'DmaPlugin', 'requestXiaomiPermsAuto', [options]);
}

DmaPlugin.prototype.clearRequestAutostart = function(successCallback, errorCallback) {
  var options = {};
  cordova.exec(successCallback, errorCallback, 'DmaPlugin', 'clearRequestAutostart', [options]);
}

DmaPlugin.prototype.clearRequestXiaomiPerms = function(successCallback, errorCallback) {
  var options = {};
  cordova.exec(successCallback, errorCallback, 'DmaPlugin', 'clearRequestXiaomiPerms', [options]);
}

DmaPlugin.prototype.openProfile = function(name, successCallback, errorCallback) {
  var options = {};
  options.name = name;
  cordova.exec(successCallback, errorCallback, 'DmaPlugin', 'openProfile', [options]);
}

DmaPlugin.prototype.setVerifiedNumber = function(pn, successCallback, errorCallback) {
  var options = {};
  options.pn = pn;
  cordova.exec(successCallback, errorCallback, 'DmaPlugin', 'setVerifiedNumber', [options]);
}

DmaPlugin.prototype.isVerifiedNumberSet = function(successCallback, errorCallback) {
  var options = {};
  cordova.exec(successCallback, errorCallback, 'DmaPlugin', 'isVerifiedNumberSet', [options]);
}

DmaPlugin.prototype.isOverlayRequired = function(successCallback, errorCallback) {
  var options = {};
  cordova.exec(successCallback, errorCallback, 'DmaPlugin', 'isOverlayRequired', [options]);
}

DmaPlugin.prototype.isOverlayGranted = function(successCallback, errorCallback) {
  var options = {};
  cordova.exec(successCallback, errorCallback, 'DmaPlugin', 'isOverlayGranted', [options]);
}

DmaPlugin.prototype.requestOverlay = function(successCallback, errorCallback) {
  var options = {};
  cordova.exec(successCallback, errorCallback, 'DmaPlugin', 'requestOverlay', [options]);
}

DmaPlugin.prototype.isProminentApproved = function(successCallback, errorCallback) {
  var options = {};
  cordova.exec(successCallback, errorCallback, 'DmaPlugin', 'isProminentApproved', [options]);
}

DmaPlugin.prototype.setProminentApproved = function(successCallback, errorCallback) {
  var options = {};
  cordova.exec(successCallback, errorCallback, 'DmaPlugin', 'setProminentApproved', [options]);
}

DmaPlugin.prototype.onFcmTokenReceived = function(token, successCallback, errorCallback) {
  var options = {};
  options.token = token;
  cordova.exec(successCallback, errorCallback, 'DmaPlugin', 'onFcmTokenReceived', [options]);
}

DmaPlugin.prototype.onFcmMessageReceived = function(payload, successCallback, errorCallback) {
  var options = {};
  options.payload = payload;
  cordova.exec(successCallback, errorCallback, 'DmaPlugin', 'onFcmMessageReceived', [options]);
}

// Installation constructor that binds DmaPlugin to window
DmaPlugin.install = function() {
  if (!window.plugins) {
    window.plugins = {};
  }
  window.plugins.dmaPlugin = new DmaPlugin();
  return window.plugins.dmaPlugin;
};
cordova.addConstructor(DmaPlugin.install);
